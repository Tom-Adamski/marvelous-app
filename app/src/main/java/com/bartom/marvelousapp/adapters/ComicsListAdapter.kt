package com.bartom.marvelousapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bartom.marvelousapp.R
import com.bartom.marvelousapp.api_entity.Comic
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.comic_list_item_match.view.*

class ComicsListAdapter(
    private val comicList: List<Comic>,
    private val onComicListener: OnComicListener,
    private val matchWidth : Boolean,
    private val goodQuality : Boolean
) : RecyclerView.Adapter<ComicsListAdapter.ComicViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComicViewHolder {
        val view =  if(matchWidth) {
            LayoutInflater.from(parent.context)
                .inflate(R.layout.comic_list_item_match, parent, false)
        } else {
            LayoutInflater.from(parent.context)
                .inflate(R.layout.comic_list_item_wrap, parent, false)
        }
        return ComicViewHolder(view, onComicListener, goodQuality)
    }

    override fun onBindViewHolder(holder: ComicViewHolder, position: Int) {
        holder.display(comicList[position])
    }

    override fun getItemCount(): Int {
        return comicList.size
    }


    class ComicViewHolder(
        itemView: View,
        private val onComicListener: OnComicListener,
        private val goodQuality: Boolean
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val name: TextView = itemView.name
        private val thumbnail: ImageView = itemView.thumbnail

        init {
            itemView.setOnClickListener(this)
        }

        fun display(comic: Comic) {
            name.text = comic.title

            val url = if(goodQuality) {
                "${comic.thumbnail.path}/portrait_xlarge.${comic.thumbnail.extension}"
            } else {
                "${comic.thumbnail.path}/portrait_medium.${comic.thumbnail.extension}"
            }

            if(comic.thumbnail != null && comic.thumbnail.path != null) {
                Glide.with(itemView)
                    .load(url)
                    .into(thumbnail)
            }
        }

        override fun onClick(view: View?) {
            onComicListener.onComicClick(adapterPosition)
        }


    }

    interface OnComicListener {
        fun onComicClick(position: Int)
    }

}