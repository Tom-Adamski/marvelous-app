package com.bartom.marvelousapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bartom.marvelousapp.R
import com.bartom.marvelousapp.api_entity.Creator
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.creator_list_item_wrap.view.*

class CreatorListAdapter(
    private val creatorList: List<Creator>,
    private val onCreatorListener: OnCreatorListener
) : RecyclerView.Adapter<CreatorListAdapter.CreatorViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CreatorViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.creator_list_item_wrap, parent, false)
        return CreatorViewHolder(view, onCreatorListener)
    }

    override fun onBindViewHolder(holder: CreatorViewHolder, position: Int) {
        holder.display(creatorList[position])
    }

    override fun getItemCount(): Int {
        return creatorList.size
    }


    class CreatorViewHolder(
        itemView: View,
        private val onCreatorListener: OnCreatorListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val name: TextView = itemView.name
        private val thumbnail: ImageView = itemView.thumbnail

        init {
            itemView.setOnClickListener(this)
        }

        fun display(creator: Creator) {
            name.text = creator.fullName

            if(creator.thumbnail != null && creator.thumbnail.path != null) {
                Glide.with(itemView)
                    .load("${creator.thumbnail.path}/portrait_medium.${creator.thumbnail.extension}")
                    .into(thumbnail)
            }
        }

        override fun onClick(view: View?) {
            onCreatorListener.onCreatorClick(adapterPosition)
        }


    }

    interface OnCreatorListener {
        fun onCreatorClick(position: Int)
    }

}