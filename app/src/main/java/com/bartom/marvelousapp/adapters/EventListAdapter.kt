package com.bartom.marvelousapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bartom.marvelousapp.R
import com.bartom.marvelousapp.api_entity.Event
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.event_list_item_wrap.view.*

class EventListAdapter(
    private val eventList: List<Event>,
    private val onEventListener: OnEventListener
) : RecyclerView.Adapter<EventListAdapter.EventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.event_list_item_wrap, parent, false)
        return EventViewHolder(view, onEventListener)
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.display(eventList[position])
    }

    override fun getItemCount(): Int {
        return eventList.size
    }


    class EventViewHolder(
        itemView: View,
        private val onEventListener: OnEventListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val name: TextView = itemView.name
        private val thumbnail: ImageView = itemView.thumbnail

        init {
            itemView.setOnClickListener(this)
        }

        fun display(event: Event) {
            name.text = event.title

            if(event.thumbnail != null && event.thumbnail.path != null) {
                Glide.with(itemView)
                    .load("${event.thumbnail.path}/portrait_medium.${event.thumbnail.extension}")
                    .into(thumbnail)
            }
        }

        override fun onClick(view: View?) {
            onEventListener.onEventClick(adapterPosition)
        }


    }

    interface OnEventListener {
        fun onEventClick(position: Int)
    }

}