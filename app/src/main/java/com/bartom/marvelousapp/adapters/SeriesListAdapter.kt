package com.bartom.marvelousapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bartom.marvelousapp.R
import com.bartom.marvelousapp.api_entity.Series
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.series_list_item_wrap.view.*

class SeriesListAdapter(
    private val seriesList: List<Series>,
    private val onSeriesListener: OnSeriesListener
) : RecyclerView.Adapter<SeriesListAdapter.SeriesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeriesViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.series_list_item_wrap, parent, false)
        return SeriesViewHolder(view, onSeriesListener)
    }

    override fun onBindViewHolder(holder: SeriesViewHolder, position: Int) {
        holder.display(seriesList[position])
    }

    override fun getItemCount(): Int {
        return seriesList.size
    }


    class SeriesViewHolder(
        itemView: View,
        private val onSeriesListener: OnSeriesListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val name: TextView = itemView.name
        private val thumbnail: ImageView = itemView.thumbnail

        init {
            itemView.setOnClickListener(this)
        }

        fun display(series: Series) {
            name.text = series.title
            if(series.thumbnail != null && series.thumbnail.path != null) {
                Glide.with(itemView)
                    .load("${series.thumbnail.path}/portrait_medium.${series.thumbnail.extension}")
                    .into(thumbnail)
            }
        }

        override fun onClick(view: View?) {
            onSeriesListener.onSeriesClick(adapterPosition)
        }


    }

    interface OnSeriesListener {
        fun onSeriesClick(position: Int)
    }

}