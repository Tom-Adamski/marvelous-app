package com.bartom.marvelousapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bartom.marvelousapp.R
import com.bartom.marvelousapp.api_entity.Character
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.character_list_item_wrap.view.*

class CharacterListAdapter(
    private val characterList: List<Character>,
    private val onCharacterListener: OnCharacterListener,
    private val matchWidth : Boolean,
    private val goodQuality : Boolean
) : RecyclerView.Adapter<CharacterListAdapter.CharacterViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val view =  if(matchWidth) {
            LayoutInflater.from(parent.context).inflate(R.layout.character_list_item_match, parent, false)
        } else {
            LayoutInflater.from(parent.context).inflate(R.layout.character_list_item_wrap, parent, false)
        }

        return CharacterViewHolder(view, onCharacterListener, goodQuality)
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.display(characterList[position])
    }

    override fun getItemCount(): Int {
        return characterList.size
    }


    class CharacterViewHolder(
        itemView: View,
        private val onCharacterListener: OnCharacterListener,
        private val goodQuality: Boolean
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val name: TextView = itemView.name
        private val thumbnail: ImageView = itemView.thumbnail

        init {
            itemView.setOnClickListener(this)
        }

        fun display(character: Character) {
            name.text = character.name

            val url = if(goodQuality) {
                "${character.thumbnail.path}/portrait_xlarge.${character.thumbnail.extension}"
            } else {
                "${character.thumbnail.path}/portrait_medium.${character.thumbnail.extension}"
            }

            if(character.thumbnail != null && character.thumbnail.path != null) {
                Glide.with(itemView)
                    .load(url)
                    .into(thumbnail)
            }
        }

        override fun onClick(view: View?) {
            onCharacterListener.onCharacterClick(adapterPosition)
        }


    }

    interface OnCharacterListener {
        fun onCharacterClick(position: Int)
    }

}