package com.bartom.marvelousapp.adapters

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class GridScrollEndListener(private val glManager: GridLayoutManager) : RecyclerView.OnScrollListener(){

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if(dx > 0 || dy > 0){
            if(glManager.childCount + glManager.findFirstVisibleItemPosition() >= glManager.itemCount){
                fetchData()
            }
        }

    }


    abstract fun fetchData()


}