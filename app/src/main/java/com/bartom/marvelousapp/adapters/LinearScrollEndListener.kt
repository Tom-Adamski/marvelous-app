package com.bartom.marvelousapp.adapters

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class LinearScrollEndListener(private val llManager: LinearLayoutManager) : RecyclerView.OnScrollListener(){

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if(dx > 0 || dy > 0){
            if(llManager.childCount + llManager.findFirstVisibleItemPosition() >= llManager.itemCount){
                fetchData()
            }
        }

    }


    abstract fun fetchData()


}