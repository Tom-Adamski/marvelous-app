package com.bartom.marvelousapp.api_entity

data class SeriesSummary (
    val resourceURI : String, // The path to the individual series resource.,
    val name : String // The canonical name of the series.
)