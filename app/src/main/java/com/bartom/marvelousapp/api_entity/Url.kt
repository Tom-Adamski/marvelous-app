package com.bartom.marvelousapp.api_entity

data class Url (
    val type : String, // A text identifier for the URL.,
    val url : String // A full URL (including scheme, domain, and path).
)