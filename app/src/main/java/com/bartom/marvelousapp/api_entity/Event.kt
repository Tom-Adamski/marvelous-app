package com.bartom.marvelousapp.api_entity

data class Event (
    val id : Int, // The unique ID of the event resource.,
    val title : String, // The title of the event.,
    val description : String, // A description of the event.,
    val resourceURI : String, // The canonical URL identifier for this resource.,
    val urls : List<Url>, // A set of public web site URLs for the event.,
    val modified : String, // The date the resource was most recently modified.,
    val start : String, // The date of publication of the first issue in this event.,
    val end : String, // The date of publication of the last issue in this event.,
    val thumbnail : Image, // The representative image for this event.,
    val comics : ComicList, // A resource list containing the comics in this event.,
    val stories : StoryList, // A resource list containing the stories in this event.,
    val series : SeriesList, // A resource list containing the series in this event.,
    val characters : CharacterList, // A resource list containing the characters which appear in this event.,
    val creators : CreatorList, // A resource list containing creators whose work appears in this event.,
    val next : EventSummary, // A summary representation of the event which follows this event.,
    val previous : EventSummary // A summary representation of the event which preceded this event.
)