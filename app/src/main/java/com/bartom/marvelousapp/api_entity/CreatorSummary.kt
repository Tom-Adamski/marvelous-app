package com.bartom.marvelousapp.api_entity

data class CreatorSummary (
    val resourceURI : String, // The path to the individual creator resource.,
    val name : String, // The full name of the creator.,
    val role : String // The role of the creator in the parent entity.
)