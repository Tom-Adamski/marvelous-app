package com.bartom.marvelousapp.api_entity

data class Series (
    val id : Int, // The unique ID of the series resource.,
    val title : String, // The canonical title of the series.,
    val description : String, // A description of the series.,
    val resourceURI : String, // The canonical URL identifier for this resource.,
    val urls : List<Url>, // A set of public web site URLs for the resource.,
    val startYear : Int, // The first year of publication for the series.,
    val endYear : Int, // The last year of publication for the series : conventionally, 2099 for ongoing series) .,
    val rating : String, // The age-appropriateness rating for the series.,
    val modified : String, // The date the resource was most recently modified.,
    val thumbnail : Image, // The representative image for this series.,
    val comics : ComicList, // A resource list containing comics in this series.,
    val stories : StoryList, // A resource list containing stories which occur in comics in this series.,
    val events : EventList, // A resource list containing events which take place in comics in this series.,
    val characters : CharacterList, // A resource list containing characters which appear in comics in this series.,
    val creators : CreatorList, // A resource list of creators whose work appears in comics in this series.,
    val next : SeriesSummary, // A summary representation of the series which follows this series.,
    val previous : SeriesSummary // A summary representation of the series which preceded this series.
)