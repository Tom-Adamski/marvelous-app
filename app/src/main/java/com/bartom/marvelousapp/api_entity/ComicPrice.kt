package com.bartom.marvelousapp.api_entity

data class ComicPrice (
    val type : String, // A description of the price : e.g. prInt price, digital price.,
    val price : Float // The price : all prices in USD.
)