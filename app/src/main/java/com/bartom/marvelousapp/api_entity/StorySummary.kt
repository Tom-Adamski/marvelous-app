package com.bartom.marvelousapp.api_entity

data class StorySummary (
    val resourceURI : String, // The path to the individual story resource.,
    val name : String, // The canonical name of the story.,
    val type : String // The type of the story (Interior or cover).
)
