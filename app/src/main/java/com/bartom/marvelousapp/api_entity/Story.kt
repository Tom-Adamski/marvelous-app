package com.bartom.marvelousapp.api_entity

data class Story (
    val id : Int, // The unique ID of the story resource.,
    val title : String, // The story title.,
    val description : String, // A short description of the story.,
    val resourceURI : String, // The canonical URL identifier for this resource. ,
    val type : String, // The story type e.g. Interior story, cover, text story.,
    val modified : String, // The date the resource was most recently modified.,
    val thumbnail : Image, // The representative image for this story.,
    val comics : ComicList, // A resource list containing comics in which this story takes place.,
    val series : SeriesList, // A resource list containing series in which this story appears.,
    val events : EventList, // A resource list of the events in which this story appears.,
    val characters : CharacterList, // A resource list of characters which appear in this story.,
    val creators : CreatorList, // A resource list of creators who worked on this story.,
    val originalissue : ComicSummary // A summary representation of the issue in which this story was originally published.
)