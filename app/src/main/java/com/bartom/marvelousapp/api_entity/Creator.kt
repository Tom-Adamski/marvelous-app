package com.bartom.marvelousapp.api_entity

data class Creator (
    val id : Int, // The unique ID of the creator resource.,
    val firstName : String, // The first name of the creator.,
    val middleName : String, // The middle name of the creator.,
    val lastName : String, // The last name of the creator.,
    val suffix : String, // The suffix or honorific for the creator.,
    val fullName : String, // The full name of the creator : a space-separated concatenation of the above four fields).,
    val modified : String, // The date the resource was most recently modified.,
    val resourceURI : String, // The canonical URL identifier for this resource.,
    val urls : List<Url>, // A set of public web site URLs for the resource.,
    val thumbnail : Image, // The representative image for this creator.,
    val series : SeriesList, // A resource list containing the series which feature work by this creator.,
    val stories : StoryList, // A resource list containing the stories which feature work by this creator.,
    val comics : ComicList, // A resource list containing the comics which feature work by this creator.,
    val events : EventList // A resource list containing the events which feature work by this creator.
)