package com.bartom.marvelousapp.api_entity

data class ComicDate (
    val type : String, // A description of the date : e.g. onsale date, FOC date).,
    val date : String // The date.
)