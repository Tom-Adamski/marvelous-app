package com.bartom.marvelousapp.api_entity

data class ComicSummary (
    val resourceURI : String, // The path to the individual comic resource.,
    val name : String // The canonical name of the comic.
)
