package com.bartom.marvelousapp.api_entity

data class EventSummary (
    val resourceURI : String, // The path to the individual event resource.,
    val name : String // The name of the event.
)