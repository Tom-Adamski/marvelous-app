package com.bartom.marvelousapp.api_entity

data class Image (
    val path : String, // The directory path of to the image.,
    val extension : String // The file extension for the image.
)