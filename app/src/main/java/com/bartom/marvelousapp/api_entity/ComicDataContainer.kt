package com.bartom.marvelousapp.api_entity

data class ComicDataContainer (
    val offset : Int, // The requested offset : number of skipped results) of the call.,
    val limit : Int, // The requested result limit.,
    val total : Int, // The total number of resources available given the current filter set.,
    val count : Int, // The total number of results returned by this call.,
    val results : List<Comic> // The list of comics returned by the call
)