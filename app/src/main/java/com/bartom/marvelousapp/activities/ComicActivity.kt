package com.bartom.marvelousapp.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bartom.marvelousapp.R
import com.bartom.marvelousapp.adapters.*
import com.bartom.marvelousapp.api.ComicsClient
import com.bartom.marvelousapp.api_entity.*
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_comic.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ComicActivity : AppCompatActivity(), CharacterListAdapter.OnCharacterListener,
    SeriesListAdapter.OnSeriesListener,
    EventListAdapter.OnEventListener, CreatorListAdapter.OnCreatorListener {

    val builder = Retrofit.Builder()
        .baseUrl("http://gateway.marvel.com")
        .addConverterFactory(GsonConverterFactory.create())

    lateinit var comic: Comic
    var comicId: Int = -1

    /* Characters */
    val charactersList = ArrayList<Character>()
    var charactersLoaded = 0
    var moreCharacters = true
    var loadingCharacters = false

    /* Stories */
    val storiesList = ArrayList<Story>()
    var storiesLoaded = 0
    var moreStories = true
    var loadingStories = false

    /* Events */
    val eventsList = ArrayList<Event>()
    var eventsLoaded = 0
    var moreEvents = true
    var loadingEvents = false

    /* Events */
    val creatorsList = ArrayList<Creator>()
    var creatorsLoaded = 0
    var moreCreators = true
    var loadingCreators = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comic)

        comicId = intent.getIntExtra(getString(R.string.extra_comic_id), -1)

        if (comicId == -1) {
            Toast.makeText(this, "No character ID", Toast.LENGTH_LONG).show()
        } else {
            loadComic()

            prepareLists()

            loadData()
        }
    }

    private fun prepareLists() {
        /* Comics List */
        val comicsLayoutManager =
            LinearLayoutManager(this@ComicActivity, LinearLayoutManager.HORIZONTAL, false)

        characters_list.layoutManager = comicsLayoutManager
        characters_list.adapter = CharacterListAdapter(charactersList, this@ComicActivity, false, false)


        characters_list.addOnScrollListener(object : LinearScrollEndListener(comicsLayoutManager) {
            override fun fetchData() {
                if (moreCharacters && !loadingCharacters) {
                    loadingCharacters = true
                    loadCharacters()
                }
            }
        })


        /* Event List */
        val eventsLayoutManager =
            LinearLayoutManager(this@ComicActivity, LinearLayoutManager.HORIZONTAL, false)

        events_list.layoutManager = eventsLayoutManager
        events_list.adapter = EventListAdapter(eventsList, this@ComicActivity)


        events_list.addOnScrollListener(object : LinearScrollEndListener(eventsLayoutManager) {
            override fun fetchData() {
                if (moreEvents && !loadingEvents) {
                    loadingEvents = true
                    loadEvents()
                }
            }
        })

        /* Creator List */
        val creatorsLayoutManager =
            LinearLayoutManager(this@ComicActivity, LinearLayoutManager.HORIZONTAL, false)

        creators_list.layoutManager = creatorsLayoutManager
        creators_list.adapter = CreatorListAdapter(creatorsList, this@ComicActivity)


        creators_list.addOnScrollListener(object : LinearScrollEndListener(creatorsLayoutManager) {
            override fun fetchData() {
                if (moreCreators && !loadingCreators) {
                    loadingCreators = true
                    loadCreators()
                }
            }
        })
    }

    private fun loadComic() {
        val retrofit = builder.build()
        val client = retrofit.create(ComicsClient::class.java)
        val call = client.Comic(comicId)


        call.enqueue(object : Callback<ComicDataWrapper> {
            override fun onResponse(
                call: Call<ComicDataWrapper>,
                response: Response<ComicDataWrapper>
            ) {
                // Affichage des résultats dans la liste
                if (response.body()?.data?.results != null) {
                    comic = response.body()!!.data.results[0]

                    displayComic()
                }

            }

            override fun onFailure(call: Call<ComicDataWrapper>, t: Throwable) {
                Toast.makeText(
                    this@ComicActivity,
                    "Erreur lors du chargement du personnage.",
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }

    fun displayComic() {
        Glide.with(this)
            .load("${comic.thumbnail.path}/portrait_fantastic.${comic.thumbnail.extension}")
            .into(thumbnail)

        name.text = comic.title

        if (comic.description != null) {
            description.text = comic.description
            description.visibility = View.VISIBLE
        }
    }

    private fun loadData() {
        loadCharacters()
        loadEvents()
        loadCreators()
    }

    private fun loadCharacters() {
        val retrofit = builder.build()
        val client = retrofit.create(ComicsClient::class.java)
        //offset = comics already loaded
        val call = client.ComicCharacters(comicId, charactersLoaded)


        call.enqueue(object : Callback<CharacterDataWrapper> {
            override fun onResponse(
                call: Call<CharacterDataWrapper>,
                response: Response<CharacterDataWrapper>
            ) {
                // Affichage des résultats dans la liste des comics
                if (response.body()?.data?.results != null) {
                    val comics = response.body()!!.data.results

                    charactersList.addAll(comics)
                    characters_list.adapter?.notifyDataSetChanged()

                    //Reste-t-il des comics à charger ?
                    charactersLoaded = response.body()!!.data.offset + response.body()!!.data.count

                    if (charactersLoaded == response.body()!!.data.total) {
                        moreCharacters = false
                    }
                    loadingCharacters = false
                }

                //Réaffichage des vues (cachées par défaut)
                if (charactersList.isNotEmpty()) {
                    characters_title.visibility = View.VISIBLE
                    characters_list.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<CharacterDataWrapper>, t: Throwable) {
                Toast.makeText(
                    this@ComicActivity,
                    "Erreur lors du chargement des comics.",
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }

    private fun loadEvents() {
        val retrofit = builder.build()
        val client = retrofit.create(ComicsClient::class.java)
        //offset = stories already loaded
        val call = client.ComicEvents(comicId, eventsLoaded)


        call.enqueue(object : Callback<EventDataWrapper> {
            override fun onResponse(
                call: Call<EventDataWrapper>,
                response: Response<EventDataWrapper>
            ) {
                // Affichage des résultats dans la liste des evts
                if (response.body()?.data?.results != null) {
                    val events = response.body()!!.data.results

                    eventsList.addAll(events)
                    events_list.adapter?.notifyDataSetChanged()

                    //Reste-t-il des evts à charger ?
                    eventsLoaded = response.body()!!.data.offset + response.body()!!.data.count

                    if (eventsLoaded == response.body()!!.data.total) {
                        moreEvents = false
                    }
                    loadingEvents = false
                }

                //Réaffichage des vues (cachées par défaut)
                if (eventsList.isNotEmpty()) {
                    events_title.visibility = View.VISIBLE
                    events_list.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<EventDataWrapper>, t: Throwable) {
                Toast.makeText(
                    this@ComicActivity,
                    "Erreur lors du chargement des événements.",
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }

    private fun loadCreators() {
        val retrofit = builder.build()
        val client = retrofit.create(ComicsClient::class.java)
        //offset = stories already loaded
        val call = client.ComicCreators(comicId, creatorsLoaded)


        call.enqueue(object : Callback<CreatorDataWrapper> {
            override fun onResponse(
                call: Call<CreatorDataWrapper>,
                response: Response<CreatorDataWrapper>
            ) {
                // Affichage des résultats dans la liste des evts
                if (response.body()?.data?.results != null) {
                    val creators = response.body()!!.data.results

                    creatorsList.addAll(creators)
                    creators_list.adapter?.notifyDataSetChanged()

                    //Reste-t-il des créateurs à charger ?
                    creatorsLoaded = response.body()!!.data.offset + response.body()!!.data.count

                    if (creatorsLoaded == response.body()!!.data.total) {
                        moreCreators = false
                    }
                    loadingCreators = false
                }

                //Réaffichage des vues (cachées par défaut)
                if (creatorsList.isNotEmpty()) {
                    creators_title.visibility = View.VISIBLE
                    creators_list.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<CreatorDataWrapper>, t: Throwable) {
                Toast.makeText(
                    this@ComicActivity,
                    "Erreur lors du chargement des événements.",
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }


    override fun onCharacterClick(position: Int) {
        val characterId = charactersList[position].id
        val characterIntent = Intent(this, CharacterActivity::class.java)
        characterIntent.putExtra(getString(R.string.extra_character_id), characterId)
        startActivity(characterIntent)
    }

    override fun onSeriesClick(position: Int) {
        println("Clicked series pos $position")
    }


    override fun onEventClick(position: Int) {
        println("Clicked event pos $position")
    }

    override fun onCreatorClick(position: Int) {
        println("Clicked creator pos $position")
    }


}
