package com.bartom.marvelousapp.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bartom.marvelousapp.R
import com.bartom.marvelousapp.adapters.ComicsListAdapter
import com.bartom.marvelousapp.adapters.EventListAdapter
import com.bartom.marvelousapp.adapters.LinearScrollEndListener
import com.bartom.marvelousapp.adapters.SeriesListAdapter
import com.bartom.marvelousapp.api.CharacterClient
import com.bartom.marvelousapp.api_entity.*
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_character.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CharacterActivity : AppCompatActivity(), ComicsListAdapter.OnComicListener,
    SeriesListAdapter.OnSeriesListener,
    EventListAdapter.OnEventListener {

    val builder = Retrofit.Builder()
        .baseUrl("http://gateway.marvel.com")
        .addConverterFactory(GsonConverterFactory.create())

    lateinit var character: Character
    var characterId: Int = -1

    /* Comics */
    val comicsList = ArrayList<Comic>()
    var comicsLoaded = 0
    var moreComics = true
    var loadingComics = false

    /* Series */
    val seriesList = ArrayList<Series>()
    var seriesLoaded = 0
    var moreSeries = true
    var loadingSeries = false

    /* Stories */
    val storiesList = ArrayList<Story>()
    var storiesLoaded = 0
    var moreStories = true
    var loadingStories = false

    /* Events */
    val eventsList = ArrayList<Event>()
    var eventsLoaded = 0
    var moreEvents = true
    var loadingEvents = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character)

        characterId = intent.getIntExtra(getString(R.string.extra_character_id), -1)

        if (characterId == -1) {
            Toast.makeText(this, "No character ID", Toast.LENGTH_LONG).show()
        } else {
            loadCharacter()

            prepareLists()

            loadData()
        }
    }

    private fun prepareLists() {
        /* Comics List */
        val comicsLayoutManager =
            LinearLayoutManager(this@CharacterActivity, LinearLayoutManager.HORIZONTAL, false)

        comics_list.layoutManager = comicsLayoutManager
        comics_list.adapter = ComicsListAdapter(comicsList, this@CharacterActivity, false, false)


        comics_list.addOnScrollListener(object : LinearScrollEndListener(comicsLayoutManager) {
            override fun fetchData() {
                if (moreComics && !loadingComics) {
                    loadingComics = true
                    loadComics()
                }
            }
        })

        /* Series List */
        val seriesLayoutManager =
            LinearLayoutManager(this@CharacterActivity, LinearLayoutManager.HORIZONTAL, false)

        series_list.layoutManager = seriesLayoutManager
        series_list.adapter = SeriesListAdapter(seriesList, this@CharacterActivity)


        series_list.addOnScrollListener(object : LinearScrollEndListener(seriesLayoutManager) {
            override fun fetchData() {
                if (moreSeries && !loadingSeries) {
                    loadingSeries = true
                    loadSeries()
                }
            }
        })


        /* Event List */
        val eventsLayoutManager =
            LinearLayoutManager(this@CharacterActivity, LinearLayoutManager.HORIZONTAL, false)

        events_list.layoutManager = eventsLayoutManager
        events_list.adapter = EventListAdapter(eventsList, this@CharacterActivity)


        events_list.addOnScrollListener(object : LinearScrollEndListener(eventsLayoutManager) {
            override fun fetchData() {
                if (moreEvents && !loadingEvents) {
                    loadingEvents = true
                    loadEvents()
                }
            }
        })
    }

    private fun loadCharacter() {
        val retrofit = builder.build()
        val client = retrofit.create(CharacterClient::class.java)
        val call = client.Character(characterId)


        call.enqueue(object : Callback<CharacterDataWrapper> {
            override fun onResponse(
                call: Call<CharacterDataWrapper>,
                response: Response<CharacterDataWrapper>
            ) {
                // Affichage des résultats dans la liste
                if (response.body()?.data?.results != null) {
                    character = response.body()!!.data.results[0]

                    displayCharacter()
                }

            }

            override fun onFailure(call: Call<CharacterDataWrapper>, t: Throwable) {
                Toast.makeText(
                    this@CharacterActivity,
                    "Erreur lors du chargement du personnage.",
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }

    fun displayCharacter() {
        Glide.with(this)
            .load("${character.thumbnail.path}/portrait_fantastic.${character.thumbnail.extension}")
            .into(thumbnail)

        name.text = character.name

        if (character.description != null) {
            description.text = character.description
            description.visibility = View.VISIBLE
        }
    }

    private fun loadData() {
        loadComics()
        loadSeries()
        loadEvents()
    }

    private fun loadComics() {
        val retrofit = builder.build()
        val client = retrofit.create(CharacterClient::class.java)
        //offset = comics already loaded
        val call = client.CharacterComics(characterId, comicsLoaded)


        call.enqueue(object : Callback<ComicDataWrapper> {
            override fun onResponse(
                call: Call<ComicDataWrapper>,
                response: Response<ComicDataWrapper>
            ) {
                // Affichage des résultats dans la liste des comics
                if (response.body()?.data?.results != null) {
                    val comics = response.body()!!.data.results

                    comicsList.addAll(comics)
                    comics_list.adapter?.notifyDataSetChanged()

                    //Reste-t-il des comics à charger ?
                    comicsLoaded = response.body()!!.data.offset + response.body()!!.data.count

                    if (comicsLoaded == response.body()!!.data.total) {
                        moreComics = false
                    }
                    loadingComics = false
                }

                //Réaffichage des vues (cachées par défaut)
                if (comicsList.isNotEmpty()) {
                    comics_title.visibility = View.VISIBLE
                    comics_list.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<ComicDataWrapper>, t: Throwable) {
                Toast.makeText(
                    this@CharacterActivity,
                    "Erreur lors du chargement des comics.",
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }

    private fun loadSeries() {
        val retrofit = builder.build()
        val client = retrofit.create(CharacterClient::class.java)
        //offset = series already loaded
        val call = client.CharacterSeries(characterId, seriesLoaded)


        call.enqueue(object : Callback<SeriesDataWrapper> {
            override fun onResponse(
                call: Call<SeriesDataWrapper>,
                response: Response<SeriesDataWrapper>
            ) {
                // Affichage des résultats dans la liste des séries
                if (response.body()?.data?.results != null) {
                    val series = response.body()!!.data.results

                    seriesList.addAll(series)
                    series_list.adapter?.notifyDataSetChanged()

                    //Reste-t-il des séries à charger ?
                    seriesLoaded = response.body()!!.data.offset + response.body()!!.data.count

                    if (seriesLoaded == response.body()!!.data.total) {
                        moreSeries = false
                    }
                    loadingSeries = false
                }

                //Réaffichage des vues (cachées par défaut)
                if (seriesList.isNotEmpty()) {
                    series_title.visibility = View.VISIBLE
                    series_list.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<SeriesDataWrapper>, t: Throwable) {
                Toast.makeText(
                    this@CharacterActivity,
                    "Erreur lors du chargement des séries.",
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }


    private fun loadEvents() {
        val retrofit = builder.build()
        val client = retrofit.create(CharacterClient::class.java)
        //offset = stories already loaded
        val call = client.CharacterEvents(characterId, eventsLoaded)


        call.enqueue(object : Callback<EventDataWrapper> {
            override fun onResponse(
                call: Call<EventDataWrapper>,
                response: Response<EventDataWrapper>
            ) {
                // Affichage des résultats dans la liste des evts
                if (response.body()?.data?.results != null) {
                    val events = response.body()!!.data.results

                    eventsList.addAll(events)
                    events_list.adapter?.notifyDataSetChanged()

                    //Reste-t-il des evts à charger ?
                    eventsLoaded = response.body()!!.data.offset + response.body()!!.data.count

                    if (eventsLoaded == response.body()!!.data.total) {
                        moreEvents = false
                    }
                    loadingEvents = false
                }

                //Réaffichage des vues (cachées par défaut)
                if (eventsList.isNotEmpty()) {
                    events_title.visibility = View.VISIBLE
                    events_list.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<EventDataWrapper>, t: Throwable) {
                Toast.makeText(
                    this@CharacterActivity,
                    "Erreur lors du chargement des événements.",
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }


    override fun onComicClick(position: Int) {
        val comicId = comicsList[position].id
        val comicIntent = Intent(this, ComicActivity::class.java)
        comicIntent.putExtra(getString(R.string.extra_comic_id), comicId)
        startActivity(comicIntent)
    }

    override fun onSeriesClick(position: Int) {
        println("Clicked series pos $position")
    }


    override fun onEventClick(position: Int) {
        println("Clicked event pos $position")
    }


}
