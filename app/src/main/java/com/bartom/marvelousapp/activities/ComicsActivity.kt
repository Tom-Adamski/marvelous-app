package com.bartom.marvelousapp.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import com.bartom.marvelousapp.R
import com.bartom.marvelousapp.adapters.ComicsListAdapter
import com.bartom.marvelousapp.adapters.GridScrollEndListener
import com.bartom.marvelousapp.api.ComicsClient
import com.bartom.marvelousapp.api_entity.Comic
import com.bartom.marvelousapp.api_entity.ComicDataWrapper
import kotlinx.android.synthetic.main.activity_comics.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ComicsActivity : AppCompatActivity(), ComicsListAdapter.OnComicListener {

    val comicsList = ArrayList<Comic>()

    val builder = Retrofit.Builder()
        .baseUrl("http://gateway.marvel.com")
        .addConverterFactory(GsonConverterFactory.create())


    /*
        Le compteur "queryChangedCount" sert à savoir combien de recherches successives on été
        lancées, en effet si on écrit rapidement "iron", 4 requêtes sont lancées,
        'i' 'ir' 'iro' 'iron'
        En comptant ces requêtes on peut effacer la liste à chaque retour de requête afin de
        n'afficher que le retour d'une requête
        TODO BUG : Si les requêtes ne reviennent pas dans l'ordre d'appel, c'est la dernière
         revenue qui est affichée

    */

    var searchQuery = ""
    var queryChangedCount = 0


    var comicsLoaded = 0
    var moreComics = true
    var loadingComics = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comics)

        val comicsLayoutManager = GridLayoutManager(this, 2)

        comic_list.layoutManager = comicsLayoutManager
        comic_list.adapter = ComicsListAdapter(comicsList, this, true, true)

        comic_list.addOnScrollListener(object : GridScrollEndListener(comicsLayoutManager) {
            override fun fetchData() {
                if (moreComics && !loadingComics) {
                    loadComics()
                }
            }
        })
        loadComics()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_with_search, menu)

        (menu.findItem(R.id.menu_search).actionView as SearchView).setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                searchQuery = newText
                queryChangedCount++
                loadComics()
                return true
            }
        })


        return true
    }

    private fun loadComics() {
        val retrofit = builder.build()
        val client = retrofit.create(ComicsClient::class.java)

        loadingComics = true

        if(queryChangedCount > 0){
            comicsLoaded = 0
            moreComics = true
        }

        val call = if (searchQuery.isEmpty()){
            client.Comics(comicsLoaded)
        }   else {
            client.ComicSearch(searchQuery, comicsLoaded)
        }

        call.enqueue(object : Callback<ComicDataWrapper> {
            override fun onResponse(
                call: Call<ComicDataWrapper>,
                response: Response<ComicDataWrapper>
            ) {

                if(queryChangedCount > 0) {
                    comicsList.clear()
                    queryChangedCount--
                }
                // Affichage des résultats dans la liste
                if (response.body()?.data?.results != null) {
                    comicsList.addAll(response.body()!!.data.results)

                    //Reste-t-il des personnages à charger ?
                    comicsLoaded = response.body()!!.data.offset + response.body()!!.data.count

                    if (comicsLoaded == response.body()!!.data.total) {
                        moreComics = false
                    }
                }

                comic_list.adapter?.notifyDataSetChanged()

                loadingComics = false
            }

            override fun onFailure(call: Call<ComicDataWrapper>, t: Throwable) {
                Toast.makeText(
                    this@ComicsActivity,
                    "Erreur lors du chargement des personnages.",
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }


    override fun onComicClick(position: Int) {
        val comic = comicsList[position]
        val intent = Intent(this, ComicActivity::class.java)
        intent.putExtra(getString(R.string.extra_comic_id), comic.id)
        startActivity(intent)
    }

}
