package com.bartom.marvelousapp.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import com.bartom.marvelousapp.R
import com.bartom.marvelousapp.adapters.CharacterListAdapter
import com.bartom.marvelousapp.adapters.GridScrollEndListener
import com.bartom.marvelousapp.api.CharacterClient
import com.bartom.marvelousapp.api_entity.Character
import com.bartom.marvelousapp.api_entity.CharacterDataWrapper
import kotlinx.android.synthetic.main.activity_characters.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class CharactersActivity : AppCompatActivity(), CharacterListAdapter.OnCharacterListener {

    val charactersList = ArrayList<Character>()

    val builder = Retrofit.Builder()
        .baseUrl("http://gateway.marvel.com")
        .addConverterFactory(GsonConverterFactory.create())


    /*
        Le compteur "queryChangedCount" sert à savoir combien de recherches successives on été
        lancées, en effet si on écrit rapidement "iron", 4 requêtes sont lancées,
        'i' 'ir' 'iro' 'iron'
        En comptant ces requêtes on peut effacer la liste à chaque retour de requête afin de
        n'afficher que le retour d'une requête
        TODO BUG : Si les requêtes ne reviennent pas dans l'ordre d'appel, c'est la dernière
         revenue qui est affichée

    */

    var searchQuery = ""
    var queryChangedCount = 0


    var charactersLoaded = 0
    var moreCharacters = true
    var loadingCharacters = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_characters)

        val charactersLayoutManager = GridLayoutManager(this, 2)

        character_list.layoutManager = charactersLayoutManager
        character_list.adapter = CharacterListAdapter(charactersList, this, true, true)

        character_list.addOnScrollListener(object : GridScrollEndListener(charactersLayoutManager) {
            override fun fetchData() {
                if (moreCharacters && !loadingCharacters) {
                    loadCharacters()
                }
            }
        })
        loadCharacters()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_with_search, menu)

        (menu.findItem(R.id.menu_search).actionView as SearchView).setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                searchQuery = newText
                queryChangedCount++
                loadCharacters()
                return true
            }
        })


        return true
    }

    private fun loadCharacters() {
        val retrofit = builder.build()
        val client = retrofit.create(CharacterClient::class.java)

        loadingCharacters = true

        if(queryChangedCount > 0){
            charactersLoaded = 0
            moreCharacters = true
        }

        val call = if (searchQuery.isEmpty()){
            client.Characters(charactersLoaded)
        }   else {
            client.CharactersSearch(searchQuery, charactersLoaded)
        }

        call.enqueue(object : Callback<CharacterDataWrapper> {
            override fun onResponse(
                call: Call<CharacterDataWrapper>,
                response: Response<CharacterDataWrapper>
            ) {

                if(queryChangedCount > 0) {
                    charactersList.clear()
                    queryChangedCount--
                }
                // Affichage des résultats dans la liste
                if (response.body()?.data?.results != null) {
                    charactersList.addAll(response.body()!!.data.results)

                    //Reste-t-il des personnages à charger ?
                    charactersLoaded = response.body()!!.data.offset + response.body()!!.data.count

                    if (charactersLoaded == response.body()!!.data.total) {
                        moreCharacters = false
                    }
                }

                character_list.adapter?.notifyDataSetChanged()

                loadingCharacters = false
            }

            override fun onFailure(call: Call<CharacterDataWrapper>, t: Throwable) {
                Toast.makeText(
                    this@CharactersActivity,
                    "Erreur lors du chargement des personnages.",
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }


    override fun onCharacterClick(position: Int) {
        val character = charactersList[position]
        val intent = Intent(this, CharacterActivity::class.java)
        intent.putExtra(getString(R.string.extra_character_id), character.id)
        startActivity(intent)
    }


}
