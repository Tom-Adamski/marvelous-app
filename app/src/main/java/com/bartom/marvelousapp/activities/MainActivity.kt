package com.bartom.marvelousapp.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bartom.marvelousapp.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        characters.setOnClickListener {
            val intent = Intent(this, CharactersActivity::class.java)
            startActivity(intent)
        }

        comics.setOnClickListener {
            val intent = Intent(this, ComicsActivity::class.java)
            startActivity(intent)
        }

        about.setOnClickListener {
            val viewGroup = findViewById<ViewGroup>(android.R.id.content)
            val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_about, viewGroup, false)
            val builder = AlertDialog.Builder(this)
            builder.setView(dialogView)
            builder.create().show()
        }


        api.setOnClickListener {
            val marvelApiUrl = "https://developer.marvel.com/"
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(marvelApiUrl)
            startActivity(intent)
        }

    }
}
