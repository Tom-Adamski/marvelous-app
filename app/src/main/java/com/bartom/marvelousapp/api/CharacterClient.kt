package com.bartom.marvelousapp.api

import com.bartom.marvelousapp.api_entity.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CharacterClient {

    @GET("v1/public/characters")
    fun Characters(
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<CharacterDataWrapper>

    @GET("v1/public/characters")
    fun CharactersSearch(
        @Query("nameStartsWith") nameStartsWith : String,
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<CharacterDataWrapper>

    @GET("v1/public/characters/{characterId}")
    fun Character(
        @Path("characterId") characterId : Int,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<CharacterDataWrapper>

    @GET("v1/public/characters/{characterId}/comics")
    fun CharacterComics(
        @Path("characterId") characterId : Int,
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<ComicDataWrapper>

    @GET("v1/public/characters/{characterId}/events")
    fun CharacterEvents(
        @Path("characterId") characterId : Int,
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<EventDataWrapper>

    @GET("v1/public/characters/{characterId}/series")
    fun CharacterSeries(
        @Path("characterId") characterId : Int,
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<SeriesDataWrapper>

    @GET("v1/public/characters/{characterId}/stories")
    fun CharacterStories(
        @Path("characterId") characterId : Int,
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<StoryDataWrapper>


    //?ts=marvelous&apikey=a5805914d49177e615d26a22248be519&hash=8ddd4c46f0717a44b1f754dc6fd2f110
    companion object {
        val TS = "marvelous"
        val API_KEY = "a5805914d49177e615d26a22248be519"
        val HASH = "8ddd4c46f0717a44b1f754dc6fd2f110"
    }

}