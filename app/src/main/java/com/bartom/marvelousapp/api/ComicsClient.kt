package com.bartom.marvelousapp.api

import com.bartom.marvelousapp.api_entity.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ComicsClient {

    @GET("v1/public/comics")
    fun Comics(
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<ComicDataWrapper>

    @GET("v1/public/comics")
    fun ComicSearch(
        @Query("titleStartsWith") titleStartsWith : String,
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<ComicDataWrapper>

    @GET("v1/public/comics/{comicId}")
    fun Comic(
        @Path("comicId") comicId : Int,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<ComicDataWrapper>

    @GET("v1/public/comics/{comicId}/characters")
    fun ComicCharacters(
        @Path("comicId") comicId : Int,
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<CharacterDataWrapper>

    @GET("v1/public/comics/{comicId}/events")
    fun ComicEvents(
        @Path("comicId") comicId : Int,
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<EventDataWrapper>


    @GET("v1/public/comics/{comicId}/stories")
    fun ComicStories(
        @Path("comicId") comicId : Int,
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<StoryDataWrapper>

    @GET("v1/public/comics/{comicId}/creators")
    fun ComicCreators(
        @Path("comicId") comicId : Int,
        @Query("offset") offset : Int = 0,
        @Query("limit") limit : Int = 20,
        @Query("ts") ts: String = TS,
        @Query("apikey") apiKey: String = API_KEY,
        @Query("hash") hash: String = HASH
    ): Call<CreatorDataWrapper>


    //?ts=marvelous&apikey=a5805914d49177e615d26a22248be519&hash=8ddd4c46f0717a44b1f754dc6fd2f110
    companion object {
        val TS = "marvelous"
        val API_KEY = "a5805914d49177e615d26a22248be519"
        val HASH = "8ddd4c46f0717a44b1f754dc6fd2f110"
    }

}